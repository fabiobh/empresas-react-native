import React, { Component } from 'react';
import axios from 'axios';
import {
  Platform,
  ActivityIndicator,
  StyleSheet,
  Dimensions,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  ImageBackground,
  TouchableNativeFeedback,
  TouchableHighlight,
  KeyboardAvoidingView,
  Keyboard,
  Modal,
  View,
  Alert,
  ScrollView
} from 'react-native';
import {Actions, Scene, Router} from 'react-native-router-flux';
var s = require('./style');

import ModalTravaTela from './subcomponents/ModalTravaTela.js';

export default class EmpresaMenu extends Component<Props> {

  render() {
    return (
      <View style={s.container}>
        <Text style={[s.instructions,s.texto18,s.marginHorizontal20]}>
          Selecione como quer visualizar as empresas
        </Text>

        <View style={{height:10}}></View>
        
        <TouchableHighlight style={[s.fundoVerde,{borderRadius:10,marginHorizontal:75}]}
          onPress={ () => { 
            Actions.empresasTodas({ uid: this.props.uid, client: this.props.client, accessToken: this.props.accessToken })
          } }
        >
          <Text style={[s.texto18,s.fonteNegrito,s.textoBranco,s.paddingVertical20,{textAlign:'center'}]}>VISUALIZAR TODAS AS EMPRESAS</Text>
        </TouchableHighlight>

        <View style={{height:10}}></View>

        <TouchableHighlight style={[s.fundoVerde,{borderRadius:10,marginHorizontal:75}]}
          onPress={ () => { 
            Actions.empresasTipos({ uid: this.props.uid, client: this.props.client, accessToken: this.props.accessToken })
          } }
        >
          <Text style={[s.texto18,s.fonteNegrito,s.textoBranco,s.paddingVertical20,{textAlign:'center'}]}>EMPRESAS SEPARADAS POR TIPOS</Text>
        </TouchableHighlight>
      </View>
    );
  }
}
