import React, { Component } from 'react'
import {
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
  TouchableNativeFeedback,
  Modal,
  ActivityIndicator,
  View
} from 'react-native';
var s = require('../style');

export default class ModalTravaTela extends Component {
  constructor(props) {
    super(props)
    
  }

  render = () => {
    const { textStyles, travaTela } = this.props;
    
    return (

      <Modal 
          visible={travaTela}
          transparent={true}
          onRequestClose={() => {
            //alert('Modal has been closed.') 
          }} >
          <View style={[s.marginHorizontal20,{flex:1,justifyContent: 'center',
        alignItems: 'center',alignSelf:'center'}]}>
            <View style={[s.fundoBranco,{paddingHorizontal:40,paddingVertical:20}]}>
              <Text style={{textAlign:'center'}} >CARREGANDO</Text>
              <ActivityIndicator size="large" color="#0000ff" />
              <Text style={{textAlign:'center'}} >Por favor, aguarde...</Text>
            </View>
          </View>
        </Modal>
      
    );
  }
}