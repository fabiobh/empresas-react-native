import React, { Component } from 'react';
import axios from 'axios';
import {
  Platform,
  ActivityIndicator,
  StyleSheet,
  Dimensions,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  Button,
  ImageBackground,
  TouchableNativeFeedback,
  TouchableHighlight,
  KeyboardAvoidingView,
  Keyboard,
  Modal,
  View,
  Alert,
  ScrollView
} from 'react-native';
import {Actions, Scene, Router} from 'react-native-router-flux';
var s = require('./style');

import ModalTravaTela from './subcomponents/ModalTravaTela.js';

export default class EmpresasTodas extends Component<Props> {
  state = {
    respostaEnterprises: [],
    respostaJson: "",
    showText: true,

  }

  mostrarTodasEmpresas = () => {
    var _self = this

    const axios = require('axios')

    urlShowEnterprises = "http://empresas.ioasys.com.br/api/v1/enterprises";

    axios({
      url: urlShowEnterprises,
      method: 'get',
      data: {},
      headers: {
          'uid': this.props.uid,
          'client': this.props.client,
          'access-token': this.props.accessToken,
          'Content-Type': 'application/json'
      }
    })
    .then((response) => {
      //console.warn(response.data.enterprises)
      this.setState({ respostaEnterprises: response.data.enterprises, showText: false})
    }) 
    .catch(err => {
      console.warn("error2: "+err);
    });

  }

  componentDidMount() {
    this.mostrarTodasEmpresas() 
  } 

  render() {
    return (
      <ScrollView >
        <Text style={s.instructions}>
          Todas as Empresas disponíveis          
        </Text>

        { this.state.showText && 
        <Text style={s.instructions}>
          Carregando dados...
        </Text>
        }

        <View >
          
          {this.state.respostaEnterprises.map((company) =>
            <TouchableHighlight style={[{borderRadius:10}]}
              onPress={ () => { 
                Actions.empresaUnica({ id:company.id , uid: this.props.uid, client: this.props.client, accessToken: this.props.accessToken })
              } }
            >
              <View key={company.id}>
                <Text>Enterprise name: {company.enterprise_name}</Text>
                <Text>City: {company.city}</Text>
                <Text>Country: {company.country}</Text>
                <Text>Type: {company.enterprise_type.enterprise_type_name}</Text>
                <View style={{height:10, borderBottomColor: 'black',borderBottomWidth: 1,}}></View>
              </View>
            </TouchableHighlight>
                                

              )
          }

        </View>

        
      </ScrollView>
    );
  }
}