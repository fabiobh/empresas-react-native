import React, { Component } from 'react';
import axios from 'axios';
import {
  Platform,
  ActivityIndicator,
  StyleSheet,
  Dimensions,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  ImageBackground,
  TouchableNativeFeedback,
  TouchableHighlight,
  KeyboardAvoidingView,
  Keyboard,
  Modal,
  View,
  Alert,
  ScrollView
} from 'react-native';
import {Actions, Scene, Router} from 'react-native-router-flux';
var s = require('./style');

import ModalTravaTela from './subcomponents/ModalTravaTela.js';

let pic = {
 uri: 'https://virtus.virtual.pucminas.br/assets/img/login_background.png'
};

export default class Login extends Component {  
  

      state={ 
        showImage: true,
        modalTravaTela: false,
        token: '',
        uid: '',
        client: '',
        //email: '',
        //senha: '',
        email: 'testeapple@ioasys.com.br',
        senha: '12341234',
      }
    
  logar = () => {
    var _self = this

    Keyboard.dismiss()

    //validação de campos que não podem ser vazios
    if(this.state.email == '' || this.state.password == '') {
      Alert.alert(
        'Erro no login',
        'Preencha os campos Usuário e Senha',
        [
          {text: 'OK', onPress: () => console.log('OK')},
        ],
        { cancelable: false }
      )
      return
    }


    const axios = require('axios')

    urlSignin = "http://empresas.ioasys.com.br/api/v1/users/auth/sign_in";
    axios.post(urlSignin, {    
      email : this.state.email,
      password : this.state.senha
    })
    .then(function (response) { 

      if(response.status == 200){
        //_self.mostrarTodasEmpresas(response)
        Actions.empresaMenu({ uid: response.headers['uid'], client: response.headers['client'], accessToken: response.headers['access-token'] })
      } 
      
    })
    .catch(function (error) {
      alert("Credenciais inválidas")
    });
  }
  
  render() {
    return (

      <View style={{  }}>
        <View style={{height:20}}></View>

        <Text style={[s.texto24,s.centralizar]}>LOGIN - Projeto Empresas</Text>

        <View style={{height:10}}></View>

        <Text style={[s.texto20,s.centralizar]} >Usuário/email:</Text>

        <TextInput
            style={[s.texto18,s.centralizar,s.textoRoxoEscuro]}
            placeholder="Coloque o email do Usuário"
            placeholderTextColor="#00FF00"
            onChangeText={(email) => {this.setState({email})}}
            value={this.state.email}
        />
        
        <View style={{height:10}}></View>

        <Text style={[s.texto20,s.centralizar]} >Senha:</Text>

        <TextInput
            style={[s.texto18,s.centralizar,s.textoRoxoEscuro]}
            placeholder="Clique e escreva a senha"
            placeholderTextColor="#0000FF"
            onChangeText={(senha) => {this.setState({senha})}}
            secureTextEntry={true}
            value={this.state.senha}
        />

        <View style={{height:10}}></View>
              
        <TouchableHighlight style={[s.fundoVerde,{borderRadius:10,marginHorizontal:75}]}
          onPress={ () => { this.logar() } }
        >
          <Text style={[s.texto18,s.fonteNegrito,s.textoBranco,s.paddingVertical20,{textAlign:'center'}]}>ACESSAR</Text>
        </TouchableHighlight>

      </View>
    

              

      
    );
  }
}

