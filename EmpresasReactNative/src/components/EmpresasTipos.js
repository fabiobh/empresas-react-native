import React, { Component } from 'react';
import axios from 'axios';
import {
  Platform,
  ActivityIndicator,
  StyleSheet,
  Dimensions,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  ImageBackground,
  TouchableNativeFeedback,
  TouchableHighlight,
  KeyboardAvoidingView,
  Keyboard,
  Modal,
  View,
  Alert,
  ScrollView
} from 'react-native';
import {Actions, Scene, Router} from 'react-native-router-flux';
var s = require('./style');

import ModalTravaTela from './subcomponents/ModalTravaTela.js';

export default class EmpresasTipos extends Component<Props> {
  state = {
    respostaEnterprises: [],
    respostaJson: "",
    showText: true,

  }

  mostrarTodasEmpresasPorTipo = () => {
    var _self = this

    const axios = require('axios')

    urlShowEnterprises = "http://empresas.ioasys.com.br/api/v1/enterprises";

    axios({
      url: urlShowEnterprises,
      method: 'get',
      data: {},
      headers: {
          'uid': this.props.uid,
          'client': this.props.client,
          'access-token': this.props.accessToken,
          'Content-Type': 'application/json'
      }
    })
    .then((response) => {
      //console.warn(response.data.enterprises)
      
      var myMap = new Map();
      for (var i = 0; i < response.data.enterprises.length; i++) {
        myMap.set( response.data.enterprises[i].enterprise_type.enterprise_type_name , response.data.enterprises[i].enterprise_type.id);
      }

      myMap = new Map([...myMap.entries()].sort());

      var arrayConvert = []
      
      for (let entry of myMap.entries()) {
          arrayConvert.push({"id":entry[1],"name":entry[0]})  
      }
      
      this.setState({ respostaEnterprises: arrayConvert, showText: false})

    }) 
    .catch(err => {
      console.warn("error2: "+err);
    });

  }

  componentDidMount() {
    this.mostrarTodasEmpresasPorTipo() 
  } 

  render() {
    return (
      <ScrollView >
        <Text style={s.instructions}>
          Tipos de Empresas existentes
        </Text>
        <Text style={s.instructions}>
          Clique em uma para ver as empresas relacionadas do tipo específico
        </Text>

        { this.state.showText && 
        <Text style={s.instructions}>
          Carregando dados...
        </Text>
        }

        <View >

          {this.state.respostaEnterprises.map((company) =>
            <TouchableHighlight style={[{}]}
              onPress={ () => { 
                Actions.empresasEspecificas({ tipo_id:company.id , uid: this.props.uid, client: this.props.client, accessToken: this.props.accessToken })
              } }
            >
              <View key={company.id}>
                <Text>{company.name}</Text>
                <View style={{height:10, borderBottomColor: 'black',borderBottomWidth: 1,}}></View>
              </View>
            </TouchableHighlight>
           
              )
          }
        </View>

        
      </ScrollView>
    );
  }
}