import React, { Component } from 'react';
import axios from 'axios';
import {
  Platform,
  ActivityIndicator,
  StyleSheet,
  Dimensions,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  ImageBackground,
  TouchableNativeFeedback,
  TouchableHighlight,
  KeyboardAvoidingView,
  Keyboard,
  Modal,
  View,
  Alert,
  ScrollView
} from 'react-native';
import {Actions, Scene, Router} from 'react-native-router-flux';
var s = require('./style');

import ModalTravaTela from './subcomponents/ModalTravaTela.js';

export default class EmpresaUnica extends Component<Props> {
  state = {
    respostaEnterprises: [],
    respostaJson: "",
    showText: true,
    id: "",
    enterprise_name: "",
    enterprise_type_name: "",
    country: "",
    city: "",
    share_price: "",
    shares: "",
    photo: "https://www.jqueryscript.net/images/jQuery-Ajax-Loading-Overlay-with-Loading-Text-Spinner-Plugin.jpg",
    description: "",

  }

  mostrarTodasEmpresasPorId = () => {
    var _self = this

    const axios = require('axios')

    urlShowEnterprises = "http://empresas.ioasys.com.br/api/v1/enterprises/"+this.props.id;

    axios({
      url: urlShowEnterprises,
      method: 'get',
      data: {},
      headers: {
          'uid': this.props.uid,
          'client': this.props.client,
          'access-token': this.props.accessToken,
          'Content-Type': 'application/json'
      }
    })
    .then((response) => {
      this.setState({ 
        enterprise_name: response.data.enterprise.enterprise_name,
        enterprise_type_name: response.data.enterprise.enterprise_type.enterprise_type_name,
        country: response.data.enterprise.country,
        city: response.data.enterprise.city,
        share_price: response.data.enterprise.share_price,
        shares: response.data.enterprise.shares,
        photo: "http://empresas.ioasys.com.br"+response.data.enterprise.photo,
        description: response.data.enterprise.description,
        showText: false
      })
      //renderItems
    }) 
    .catch(err => {
      console.warn("error2: "+err);
    });

  }

  componentDidMount() {
    this.mostrarTodasEmpresasPorId() 
  } 

  render() {
    return (
      <ScrollView >
        <Text >
          Informações da Empresa 
        </Text>

        { this.state.showText && 
        <Text >
          Carregando dados...
        </Text>
        }


        <View key={this.props.id}>
          <Text>Enterprise name: {this.state.enterprise_name}</Text>
          <Text>Enterprise Type Name: {this.state.enterprise_type_name}</Text>
          <Text>Country: {this.state.country}</Text>
          <Text>City: {this.state.city}</Text>
          <Text>Share Price: {this.state.share_price}</Text>
          <Text>Shares: {this.state.shares}</Text>
          <Text>Description: {this.state.description}</Text>
          <Image
            style={{width: 200, height: 200}}
            source={{uri: this.state.photo}}
          />
        </View>


        
      </ScrollView>
    );
  }
}