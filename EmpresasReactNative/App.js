import React, {Component} from 'react';

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  TextInput,
  Text,
  TouchableHighlight,
  Alert,
  StatusBar,
  Keyboard,
} from 'react-native';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';

import {
  Scene,
  Router,
  Actions
} from 'react-native-router-flux';

import Login from './src/components/Login';
import EmpresaMenu from './src/components/EmpresaMenu';
import EmpresaUnica from './src/components/EmpresaUnica';
import EmpresasTodas from './src/components/EmpresasTodas';
import EmpresasTipos from './src/components/EmpresasTipos';
import EmpresasEspecificas from './src/components/EmpresasEspecificas';


class App extends React.Component {
  
  render() {
      return (
        <Router navBarButtonColor='#fff' navigationBarStyle={{ backgroundColor: '#17052f' }} titleStyle={{color: '#00c7b8'}} >
          <Scene key="root" >
            <Scene key='login'        component={Login}  hideNavBar initial={true} />
            <Scene key='empresaMenu'        component={EmpresaMenu}  hideNavBar  />

            <Scene key='empresaUnica'        component={EmpresaUnica}  hideNavBar  />
            <Scene key='empresasTodas'        component={EmpresasTodas}  hideNavBar  />
            <Scene key='empresasTipos'        component={EmpresasTipos}  hideNavBar  />

            <Scene key='empresasEspecificas'        component={EmpresasEspecificas}  hideNavBar  />


          </Scene>
        </Router>



      );

    
  }

}

export default App;